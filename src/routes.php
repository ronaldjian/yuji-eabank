<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\DomCrawler\Crawler;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/{start}/{end}', function (Request $request, Response $response, array $args) use ($container) {
        // Sample log message
        // $container->get('logger')->info("Slim-Skeleton '/' route");
        
        $query = [];
        parse_str($request->getUri()->getQuery(), $query);
        if (isset($query['page'])) unset($query['page']);

        $parameters = [
            'pt' => $request->getQueryParam('pt') ?? 4,
            'p' => $request->getQueryParam('page') ?? 1,
            // 'ts' => 212,
            'id' => 2536641,
            'l' => 'a',
            'invitation' => '',
            'start' => $args['start'],
            'end' => $args['end'],
            'sb' => $request->getQueryParam('sb') ?? 28,
            'st' => $request->getQueryParam('st') ?? 2,
            'magicNumbers' => '1531132839,1540179314,1530750479,1540209734,1530780574,1539724115,1550091423',
            'symbols' => '',
            'types' => '0,1,2,4,19,5',
            'orderTagList' => '',
            'daysList' => '1,2,3,4,5,6,7',
            'hoursList' => '0,7,14,21,1,8,15,22,2,9,16,23,3,10,17,4,11,18,5,12,19,6,13,20',
            'buySellList' => '0,1,6,16',
            'yieldStart' => '',
            'yieldEnd' => '',
            'netProfitStart' => '',
            'netProfitEnd' => '',
            'durationStart' => '',
            'durationEnd' => '',
            'takeProfitStart' => '',
            'takeProfitEnd' => '',
            'stopLoss' => '',
            'stopLossEnd' => '',
            'sizingStart' => '',
            'sizingEnd' => '',
            'selectedTime' => 1,
            'pipsStart' => '',
            'pipsEnd' => '',
            // 'z' => '0.30527417595218376',
        ];

        $content = file_get_contents('https://www.myfxbook.com/paging.html?'.http_build_query($parameters, '', '&', PHP_QUERY_RFC3986));
        
        // print_r($content);
        // exit;

        $crawler = new Crawler($content);

        // remove all first child of every tr
        // because every first child is image
        $crawler->filter('tr td:first-child, tr th:first-child')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $thead = $crawler->filter('th a')->each(function (Crawler $crawler) {
            return $crawler->text();
        });

        $tbody = [];
        $crawler->filter('tr')->each(function (Crawler $crawler) use (&$tbody) {
            if ($crawler->children()->nodeName() == 'td') {
                $tbody[] = $crawler->filter('td:not([style="display:none"])')->each(function (Crawler $crawler) {
                    return $crawler->text();
                });
            }
        });

        $pagination = $crawler->filter('div[class="alignR margin5"] a')->each(function (Crawler $crawler) {
            return $crawler->text();
        });

        $paginator = [];
        if (isset($pagination[count($pagination)-2])) {
            $paginator = range(1, $pagination[count($pagination)-2]);
        }

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', [
            'thead' => $thead,
            'tbody' => $tbody,
            'paginator' => $paginator,
            'basePath' => $request->getUri()->getBasePath(),
            'query' => http_build_query($query, '', '&', PHP_QUERY_RFC3986),
            'args' => $args
        ]);
    });
};
